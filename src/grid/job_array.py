#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess
import os
import re
import time
from datetime import datetime

from qstat import Qstat


class Job_array(object):

  class Run_mode(object):

    LOCALLY = 0
    ON_CLUSTER = 1

  class State(object):

    UNKNOWN = 0
    PRIOR_SUCCESS = 1
    PENDING = 2
    FAIL = 3
    SUCCESS = 4

  def __init__(
    self,
    memory_in_gb,
    time_in_minutes,
    ntasks,
    name,
    run_mode,
    code,
    finished_function,
    nretries,
    ncores=1,
    max_running_tasks=-1,
    caption=None,
  ):

    self.memory_in_gb = memory_in_gb
    self.time_in_minutes = time_in_minutes
    self.ntasks = ntasks
    self.name = name
    self.run_mode = run_mode
    self.code = code
    self.finished_function = finished_function
    self.nretries = nretries
    self.ncores = ncores
    self.max_running_tasks = max_running_tasks
    self.caption = caption

    if self.run_mode == Job_array.Run_mode.LOCALLY:
      self.counting_retries = 30
      self.counting_delay = 2
    elif self.run_mode == Job_array.Run_mode.ON_CLUSTER:
      self.counting_retries = 60
      self.counting_delay = 2
    else:
      raise RuntimeError('Unhandled run mode: %s' % repr(self.run_mode))

    self.state = Job_array.State.UNKNOWN
    self.unfinished_task_ids = range(0, self.ntasks)
    self.messenger = None  # will be set by Grid

    # will be set if executed on cluster, refers to the very last submission
    self.jobid = None

  def write_script(self, path, log_dir, error_log_dir):

    memory_in_mb = self.memory_in_gb * 1000
    hours = self.time_in_minutes // 60
    minutes = self.time_in_minutes - hours * 60

    ncores_str = '####$ -pe multicore 1'
    if self.ncores > 1:
      ncores_str = '#$ -pe multicore %d' % self.ncores

    mz_task_ids_str = ' '.join(['%d' % task_id for task_id in self.unfinished_task_ids])

    max_running_tasks_str = '####$ -tc 0'
    if self.max_running_tasks > 0:
      max_running_tasks_str = '#$ -tc %d' % self.max_running_tasks

    script = \
      """#!/bin/bash
#$ -S /bin/bash
#$ -N %(name)s
#$ -t 1-%(nunfinished_task_ids)d
%(max_running_tasks_str)s
#$ -l h_rt=%(hours)d:%(minutes)d:00
#$ -l h_vmem=%(memory_in_mb)dM
#$ -o %(log_dir)s
#$ -e %(error_log_dir)s
%(ncores_str)s

UNFINISHED_MZ_TASK_IDS=(%(mz_task_ids_str)s)
MZ_TASK_ID=${UNFINISHED_MZ_TASK_IDS[$(expr "${SGE_TASK_ID}-1")]}

%(code)s
""" \
      % {
      'name': self.name,
      'nunfinished_task_ids': len(self.unfinished_task_ids),
      'hours': hours,
      'minutes': minutes,
      'memory_in_mb': memory_in_mb,
      'max_running_tasks_str': max_running_tasks_str,
      'log_dir': log_dir,
      'error_log_dir': error_log_dir,
      'ncores_str': ncores_str,
      'mz_task_ids_str': mz_task_ids_str,
      'code': self.code,
    }

    fid = open(path, 'wt')
    fid.write(script)
    fid.close()
    os.chmod(path, 0755)

  def initialize_state(self):
    self.update_unfinished_task_ids()
    if len(self.unfinished_task_ids) == 0:
      self.state = Job_array.State.PRIOR_SUCCESS
    else:
      self.state = Job_array.State.PENDING

  def update_unfinished_task_ids(self):
    """
    assumes there is no latency, i.e., the state returned by the
    finished_function is expected to tally up with the reality.

    updates self.unfinished_task_ids
    """

    self.unfinished_task_ids = []
    for task_id in range(0, self.ntasks):
      if not self.finished_function(task_id):
        self.unfinished_task_ids.append(task_id)

  def update_unfinished_task_ids_with_latency(self):
    """
    assumes that all the tasks succeeded, but that there was some latency, so
    that the result of the finished function did not return correct results.

    retries to call self.get_unfinished_task_id() for self.counting_retries number of times and 
    returns the final unfinished_task_ids as array.
    """

    unfinished_task_ids_set = set(self.unfinished_task_ids)
    counting_retry = 0

    while len(unfinished_task_ids_set) > 0:
      finished_task_ids = []
      for task_id in unfinished_task_ids_set:
        if self.finished_function(task_id):
          finished_task_ids.append(task_id)

      for task_id in finished_task_ids:
        unfinished_task_ids_set.remove(task_id)

      counting_retry += 1
      if counting_retry > self.counting_retries:
        break

      time.sleep(self.counting_delay)

    self.unfinished_task_ids = list(unfinished_task_ids_set)

  def print_executing(self, run_mode_str):
    title_lines = []

    now_str = datetime.now().strftime('%Y-%m-%d %H:%M')
    if self.jobid is None:
      title_lines.append("Executing %s the job array '%s' at %s" % (run_mode_str, self.name, now_str))
    else:
      title_lines.append("Executing %s the job array '%s' with job id %s at %s" % (run_mode_str, self.name, now_str))

    if self.caption != None:
      title_lines.extend(self.caption.split('\n'))

    self.messenger.print_title(title_lines)

  def print_prior_success(self):
    title_lines = []

    now_str = datetime.now().strftime('%Y-%m-%d %H:%M')
    title_lines.append("The job array '%s' has already finished in a prior run.  Status checked at %s." % (self.name,
                       now_str))

    if self.caption != None:
      title_lines.extend(self.caption.split('\n'))

    self.messenger.print_title(title_lines)

  def print_ancestor_fail(self):
    title_lines = []
    now_str = datetime.now().strftime('%Y-%m-%d %H:%M')
    title_lines.append("The job array '%s' is a descendant of a failed parent.  It will not be run. Status checked at "
                       "%s." % (self.name, now_str))

    if self.caption != None:
      title_lines.extend(self.caption.split('\n'))

    self.messenger.print_title(title_lines)

  def print_success(self):
    title_lines = []
    now_str = datetime.now().strftime('%Y-%m-%d %H:%M')

    if self.jobid is None:
      title_lines.append("The job array '%s' successfully completed. Status checked at %s" % (self.name, now_str))
    else:
      title_lines.append("The job array '%s' with job id %s successfully completed. Status checked at %s" % (self.name,
                         self.jobid, now_str))

    if self.caption != None:
      title_lines.extend(self.caption.split('\n'))

    self.messenger.print_title(title_lines)

  def print_fail(self):
    retries_str = '%d retries' % self.nretries
    if self.nretries == 1:
      retries_str = '1 retry'

    title_lines = []
    title_lines.append("The job array '%s' failed after %s. %d out of %d task(s) failed." % (self.name, retries_str,
                       len(self.unfinished_task_ids), self.ntasks))
    title_lines.append('Status checked at %s.' % datetime.now().strftime('%Y-%m-%d %H:%M'))

    if self.caption != None:
      title_lines.extend(self.caption.split('\n'))

    self.messenger.print_title(title_lines)

  def execute_locally(self, script_name, script_dir, log_dir, error_log_dir):
    assert self.state == Job_array.State.PENDING

    self.jobid = None
    self.print_executing('locally')

    for retry in range(1, self.nretries + 1):

      if retry == 1:
        script_path = os.path.join(script_dir, '%s.grid' % script_name)
      else:
        script_path = os.path.join(script_dir, '%s_retry_%d.grid' % (script_name, retry))

      self.write_script(script_path, log_dir, error_log_dir)

      for sge_task_id in range(1, len(self.unfinished_task_ids) + 1):
        my_env = os.environ.copy()
        my_env['SGE_TASK_ID'] = '%d' % sge_task_id
        subprocess.call(script_path, env=my_env)

      self.update_unfinished_task_ids_with_latency()

      if len(self.unfinished_task_ids) == 0:
        self.state = Job_array.State.SUCCESS
        return
      else:
        self.messenger.say("Job array '%s' did not finish, %d task(s) failed to complete.  Retrying." % (self.name,
                           len(self.unfinished_task_ids)))

    self.state = Job_array.State.FAIL

  def execute_on_cluster(self, script_name, script_dir, log_dir, error_log_dir):
    assert self.state == Job_array.State.PENDING

    self.jobid = None

    self.print_executing('on cluster')

    for retry in range(1, self.nretries + 1):
      if retry == 1:
        script_path = os.path.join(script_dir, '%s.grid' % script_name)
      else:
        script_path = os.path.join(script_dir, '%s_retry_%d.grid' % (script_name, retry))

      self.write_script(script_path, log_dir, error_log_dir)

      qsub_output = subprocess.check_output(['qsub', script_path])

      match = re.match(r'^Your job-array ([0-9]+)\.([0-9]+)-([0-9]+)\:([0-9]+) \(\"[a-zA-Z_.0-9]+\"\)', qsub_output)

      if match == None:
        raise RuntimeError('Qsub output could not be parsed: %s' % qsub_output)

      now_str = datetime.now().strftime('%Y-%m-%d %H:%M')
      self.jobid = match.group(1)
      self.messenger.say("Submitted the job array '%s' with job id %s at %s" % (self.name, self.jobid, now_str))

      self.wait_for(self.jobid)

      self.update_unfinished_task_ids_with_latency()

      if len(self.unfinished_task_ids) == 0:
        self.state = Job_array.State.SUCCESS
        return
      else:
        self.messenger.say("Job array '%s' with job id %s did not finish at %s, %d task(s) failed to complete.  "
                           "Retrying." % (self.name, self.jobid, now_str, len(self.unfinished_task_ids)))

    self.state = Job_array.State.FAIL

  def parse_qstat(self, jobid):
    qstat_output = subprocess.check_output(['qstat', '-xml'])

    qstat = Qstat()
    qstat.fromstring(qstat_output, jobid)
    return qstat

  def wait_for(self, jobid):
    qstat = self.parse_qstat(jobid)
    has_pending = qstat.has('r') or qstat.has('qw') or qstat.has('t')

    error_msg = (
        "There are jobs waiting in error state for the job array '%s' " +
        "with job id %s. Not waiting for it any longer." % (
          self.name, jobid))

    if qstat.has('Eqw'):
      self.messenger.say(error_msg)
      return

    while has_pending:
      qstat = self.parse_qstat(jobid)
      if qstat.has('Eqw'):
        self.messenger.say(error_msg)
        return

      has_pending = qstat.has('r') or qstat.has('qw')
      time.sleep(3)

  def execute(self, script_name, script_dir, log_dir, error_log_dir):

    if self.run_mode == Job_array.Run_mode.LOCALLY:
      self.execute_locally(script_name, script_dir, log_dir, error_log_dir)
    elif self.run_mode == Job_array.Run_mode.ON_CLUSTER:
      self.execute_on_cluster(script_name, script_dir, log_dir, error_log_dir)
    else:
      raise RuntimeError('Unhandled run mode: %s' % repr(self.run_mode))
