#!/usr/bin/python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as et


class Qstat(object):

  def __init__(self):
    self.state_map = {}

  def fromstring(self, qstat_output,jobid):
    root = et.fromstring(qstat_output)

    for job_list in root.findall('*/job_list'):
      job_number = job_list.find('JB_job_number').text
      state = job_list.find('state').text
      if job_number == jobid:
        self.state_map[state] = True

  def has(self,state):
    if state not in self.state_map:
      return False
    else:
      return self.state_map[state]



