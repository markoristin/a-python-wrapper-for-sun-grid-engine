#!/usr/bin/python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    print "Could not find Python package: setuptools"
    from distutils.core import setup

setup(
    name='grid',
    version='0.1',
    author='Marko Ristin',
    author_email='marko.ristin@gmail.com',
    packages=['grid'],
    #scripts=[],
    url='https://bitbucket.org/markoristin/a-python-wrapper-for-sun-grid-engine',
    description='This piece of software is a wrapper around the bash commands of the Sun Grid Engine (SGE).',
    license = "GPLv3",
    #long_description=open('README.txt').read(),
)
